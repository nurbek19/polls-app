const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Question = require('../models/Question');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.get('/', (req, res) => {
        if (req.query.userID) {
            Question.find({userID: req.query.userID})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Question.find()
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/:id', (req, res) => {
        Question.find({_id: req.params.id})
            .then(results => res.send(results[0]))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user'), upload.single('image')], async (req, res) => {
        let question = req.body;

        question.userID = req.user._id;

        if (req.file) {
            question.image = req.file.filename;
        } else {
            question.image = 'noImage.png';
        }

        question.answers = JSON.parse(question.answers);

        const q = new Question(question);

        q.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.put('/:id', [auth, permit('user')], async (req, res) => {
        const answerID = req.body.answer;
        const userID = req.body.user;
        const question = await Question.findOne({_id: req.params.id});

        let userVoted = false;

        question.users.forEach(user => user === userID ? userVoted = true : null);

        if (userVoted) {
            res.send({error: 'You have already voted'});
        } else {
            question.answers = question.answers.map(answer => {
                if (JSON.stringify(answer._id) === JSON.stringify(answerID)) {
                    answer.vote += 1;
                    question.users.push(userID);
                    return answer;
                } else {
                    return answer;
                }
            });

            question.save()
                .then(artists => res.send(artists))
                .catch(error => res.status(400).send(error));
        }
    });

    router.delete('/:id', [auth, permit('user')], async (req, res) => {

        await Question.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    return router;
};

module.exports = createRouter;