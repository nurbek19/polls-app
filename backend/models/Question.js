const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
    text: {
        type: String, required: true
    },
    vote: {
        type: Number, default: 0
    }
});

const QuestionSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    image: String,
    answers: {
        type: [AnswerSchema]
    },
    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    users: {
        type: Array
    }
});

const Question = mongoose.model('Question', QuestionSchema);

module.exports = Question;