import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import PollsForm from "../../components/PollsForm/PollsForm";

class NewPoll extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>New poll</PageHeader>
                <PollsForm/>
            </Fragment>
        );
    }
}

export default NewPoll;