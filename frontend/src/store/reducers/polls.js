import {
    FETCH_COMMENTS_SUCCESS, FETCH_MY_POLLS_SUCCESS, FETCH_POLL_SUCCESS,
    FETCH_POLLS_SUCCESS
} from "../actions/actionTypes";
const initialState = {
    polls: [],
    poll: null,
    comments: [],
    myPolls: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POLLS_SUCCESS:
            return {...state, polls: action.polls};
        case FETCH_POLL_SUCCESS:
            return {...state, poll: action.poll};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        case FETCH_MY_POLLS_SUCCESS:
            return {...state, myPolls: action.polls};
        default:
            return state;
    }
};

export default reducer;