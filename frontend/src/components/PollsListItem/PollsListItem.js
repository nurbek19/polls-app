import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const PollsListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Col xs={4} md={3}>
            <Thumbnail src={image}>
                <Link to={`polls/${props.id}`}><h3>{props.title}</h3></Link>
                {props.remove && <Button bsStyle="danger" onClick={props.remove}>Remove</Button>}
            </Thumbnail>
        </Col>
    );
};

PollsListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default PollsListItem;