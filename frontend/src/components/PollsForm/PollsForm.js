import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";
import {connect} from "react-redux";
import {createPoll} from "../../store/actions/polls";

class PollsForm extends Component {
    state = {
        title: '',
        image: '',
        answers: [{answer1: ''}, {answer2: ''}]
    };

    submitFormHandler = event => {
        event.preventDefault();

        let state = {...this.state};

        state.answers = state.answers.map(answer => {return {text: Object.values(answer)[0]}});
        state.answers = JSON.stringify(state.answers);
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        this.props.onSubmit(formData, this.props.token);
    };

    titleChangeHandler = event => {
        this.setState({title: event.target.value});
    };

    inputChangeHandler = event => {
        const answers = [...this.state.answers];
        let index;
        answers.forEach((answer, key) => event.target.name === Object.keys(answer)[0] ? index = key : null);
        answers[index][event.target.name] = event.target.value;
        this.setState({answers});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    addAnswer = () => {
        let answers = [...this.state.answers];
        answers.push({["answer" + (this.state.answers.length + 1)]: ''});
        this.setState({answers});
    };

    reduceAnswer = () => {
        const answers = [...this.state.answers];
        answers.pop();
        this.setState({answers});
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Poll question"
                    type="text"
                    value={this.state.title}
                    changeHandler={this.titleChangeHandler}
                    required
                />

                <FormElement
                    propertyName="image"
                    title="Poll image"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                />

                {this.state.answers.map((answer, key) =>
                    <FormElement
                        key={key}
                        propertyName={Object.keys(answer)[0]}
                        title={'Answer ' + (key + 1)}
                        type="text"
                        value={this.state.answers[Object.keys(answer)[0]]}
                        changeHandler={this.inputChangeHandler}
                        required
                    />
                )}

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit" style={{marginRight: '20px'}}>Save</Button>
                        <Button bsStyle="success" onClick={this.addAnswer}>Add answer</Button>
                        {this.state.answers.length > 2 && <Button bsStyle="danger" onClick={this.reduceAnswer}>Reduce answer</Button>}
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    token: state.users.token
});

const mapDispatchToProps = dispatch => ({
    onSubmit: (formData, token) => dispatch(createPoll(formData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(PollsForm);
